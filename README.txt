This module is built on top of Content Sync. Content Sync module allows to export content from one Drupal Environment to another. While the module provides certain options to export Single item, Content per type or Entire content at once.

This module focuses to export contents based on Groups. Both Group and Content Sync are required to use this module.