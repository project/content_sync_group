<?php

namespace Drupal\content_sync_group\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\content_sync\ContentSyncManagerInterface;
use Drupal\content_sync\Exporter\ContentExporterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\content_sync\Form\ContentExportTrait;

/**
 * Class ContentGroupExportForm. Adds Content Export group operation.
 */
class ContentGroupExportForm extends FormBase {

  use ContentExportTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\content_sync\Exporter\ContentExporterInterface
   */
  protected $contentExporter;

  /**
   * @var \Drupal\content_sync\ContentSyncManagerInterface
   */
  protected $contentSyncManager;

  /**
   * ContentExportForm constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ContentExporterInterface $content_exporter, ContentSyncManagerInterface $content_sync_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->contentExporter = $content_exporter;
    $this->contentSyncManager = $content_sync_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('content_sync.exporter'),
      $container->get('content_sync.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_group_export_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Delete the content tar file in case an older version exist.
    file_unmanaged_delete(file_directory_temp() . '/content.tar.gz');

    // Set batch operations by entity type/bundle.
    $entities_list = [];
    $id = \Drupal::routeMatch()->getParameter('group_id');
    $group = \Drupal::entityTypeManager()->getStorage('group')->load($id);
    $entities_list = [];

    // Group.
    $entities_list[] = [
      'entity_type' => 'group',
      'entity_id' => $id,
    ];

    // Group Content.
    $group_contents = $group->getContent();

    foreach ($group_contents as $content) {
      $entities_list[] = [
        'entity_type' => 'group_content',
        'entity_id' => $content->id(),
      ];
    }

    if (!empty($entities_list)) {
      $serializer_context['export_type'] = 'tar';
      $serializer_context['include_files'] = 'folder';
      $serializer_context['include_dependencies'] = 1;
      $batch = $this->generateExportBatch($entities_list, $serializer_context);
      batch_set($batch);
    }
  }

  /**
   * @{@inheritdoc}
   */
  protected function getEntityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * @{@inheritdoc}
   */
  protected function getContentExporter() {
    return $this->contentExporter;
  }

  /**
   * @{@inheritdoc}
   */
  protected function getExportLogger() {
    return $this->logger('content_sync_groups');
  }

}
